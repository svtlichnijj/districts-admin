<?php

namespace EvolveAdmin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $user = $request->user();

	    if (!isset($user)) {
		    return redirect('admin/login');
	    }

	    if (!$user->isAdmin()) {
		    return redirect('/');
	    }

	    return $next($request);
    }
}
