<?php

namespace EvolveAdmin\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Xinax\LaravelGettext\Facades\LaravelGettext;

class FrontendBackend
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if (!empty(Route::current()->getPrefix())) {
		    if (Route::current()->getPrefix() == '/admin') {
			    LaravelGettext::setDomain('backend');
		    } else {
			    LaravelGettext::setDomain('frontend');
		    }
	    } else {
		    LaravelGettext::setDomain('message');
	    }

        return $next($request);
    }
}
