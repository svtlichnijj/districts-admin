<?php

namespace EvolveAdmin\Http\Controllers\Site;

use EvolveAdmin\Http\Controllers\Controller;

class HomeController extends Controller
{
	public function index()
	{
		return view('frontend.welcome');
	}
}