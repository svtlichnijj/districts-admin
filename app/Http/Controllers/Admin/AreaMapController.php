<?php

namespace EvolveAdmin\Http\Controllers\Admin;

use EvolveAdmin\AreaMap;
use EvolveAdmin\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AreaMapController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index(): View
	{
		$areaMaps = AreaMap::all();

		return view('admin.area_maps.index', compact('areaMaps'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return View
	 */
	public function create(): View
	{
		return view('admin.area_maps.create');
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(Request $request): RedirectResponse
	{
		$request->validate([
			'scale' => 'required',
			'size_x' => 'required',
			'size_y' => 'required',
			'x' => 'required',
			'y' => 'required',
		]);

		$areaMap = new AreaMap([
			'scale' => $request->get('scale'),
			'size_x' => $request->get('size_x'),
			'size_y' => $request->get('size_y'),
			'map_data' => json_encode([1, 2, 3, 4]),
			'x' => $request->get('x'),
			'y' => $request->get('y'),
		]);

		$areaMap->save();

		return redirect(route('area_maps.index'))->with('success', 'Area map created!');
	}

	/**
	 * Display the specified resource.
	 */
	public function show(int $id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 */
	public function edit(int $id): View
	{
		$areaMap = AreaMap::find($id);

		return view('admin.area_maps.edit', compact('areaMap'));
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(Request $request, int $id): RedirectResponse
	{
		$request->validate([
			'scale' => 'required',
			'size_x' => 'required',
			'size_y' => 'required',
			'x' => 'required',
			'y' => 'required',
		]);

		$areaMap = AreaMap::find($id);
		$areaMap->scale = $request->get('scale');
		$areaMap->size_x = $request->get('size_x');
		$areaMap->size_y = $request->get('size_y');
		$areaMap->y = $request->get('y');
		$areaMap->save();

		return redirect(route('area_maps.index'))->with('success', _i('Area map \'%d\' updated!', $id));
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(int $id): RedirectResponse
	{
		$areaMap = AreaMap::find($id);
		$areaMap->delete();

		return redirect(route('area_maps.index'))->with('success', _i('Area map \'%d\' was deleted!', $id));
	}
}
