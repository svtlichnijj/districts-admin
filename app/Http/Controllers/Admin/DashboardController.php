<?php

namespace EvolveAdmin\Http\Controllers\Admin;

use EvolveAdmin\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function index()
	{
		return view('admin.dashboard');
	}

}