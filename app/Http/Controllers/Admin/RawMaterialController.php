<?php

namespace EvolveAdmin\Http\Controllers\Admin;

use EvolveAdmin\Http\Controllers\Controller;
use EvolveAdmin\RawMaterial;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class RawMaterialController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index(): View
	{
		$rawMaterials = RawMaterial::all();

		return view('admin.raw_materials.index', compact('rawMaterials'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return View
	 */
	public function create(): View
	{
		return view('admin.raw_materials.create');
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(Request $request): RedirectResponse
	{
		$request->validate([
			'name' => 'required',
			'prevalence' => 'required',
		]);
		$filename = '';

		if ($file = $request->file('skin_file')) {
			$filename = $request->get('skin_file_name') ?
				$request->get('skin_file_name') . '.' . $file->getClientOriginalExtension() :
				$file->getClientOriginalName();
		}

		$rawMaterial = new RawMaterial([
			'name' => $request->get('name'),
			'prevalence' => $request->get('prevalence'),
			'skin_file_name' => $filename,
		]);

		$rawMaterial->save();

		if (!empty($filename) && !empty($rawMaterial->id)) {
			$file->storeAs('raw-material' . DIRECTORY_SEPARATOR . $rawMaterial->id, $filename);
		}

		return redirect(route('raw_materials.index'))
			->with('success', _i('Raw material \'%s\' (№ %d) created!', [$rawMaterial->name, $rawMaterial->id]));
	}

	/**
	 * Display the specified resource.
	 */
	public function show(int $id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 */
	public function edit(int $id): View
	{
		$rawMaterial = RawMaterial::find($id);

		return view('admin.raw_materials.edit', compact('rawMaterial'));
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(Request $request, int $id): RedirectResponse
	{
		$request->validate([
			'name' => 'required',
			'prevalence' => 'required',
		]);
		$fileDirectory = 'raw-material' . DIRECTORY_SEPARATOR . $id;
		$filename = '';

		$file = $request->file('skin_file');

		if ( ! empty($file)) {
			$filename = ! empty($request->get('skin_file_name')) ?
				$request->get('skin_file_name') . '.' . $file->getClientOriginalExtension() :
				$file->getClientOriginalName();
			$path = $file->storeAs($fileDirectory, $filename);

			if ( ! empty($filename) && file_exists($path)) {
				Storage::delete($fileDirectory . DIRECTORY_SEPARATOR . $request->get('skin_file_name_old'));
			}
		} elseif ( ! empty($request->get('skin_file_name')) && $request->get('skin_file_name') != $request->get('skin_file_name_old')) {
			if (Storage::move($fileDirectory . DIRECTORY_SEPARATOR . $request->get('skin_file_name_old'),
				$fileDirectory . DIRECTORY_SEPARATOR . $request->get('skin_file_name'))) {
				$filename = $request->get('skin_file_name');
			}
		}

		$rawMaterial = RawMaterial::find($id);
		$rawMaterial->name = $request->get('name');
		$rawMaterial->prevalence = $request->get('prevalence');

		if ( ! empty($filename)) {
			$rawMaterial->skin_file_name = $filename;
		}

		$rawMaterial->save();

		return redirect(route('raw_materials.index'))->with('success', _i('Raw material \'%d\' updated!', $id));
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Request $request, int $id): RedirectResponse
	{
		$rawMaterialName = $request->get('name');
		$rawMaterial = RawMaterial::find($id);

		if (empty($rawMaterial->id)) {
			return redirect(route('raw_materials.index'))
				->withErrors([_i('Raw material \'%s\' (№ %d) can not found!', [$rawMaterialName, $id])]);
		}

		if (!empty($rawMaterial->delete())) {
			Storage::deleteDirectory('raw-material' . DIRECTORY_SEPARATOR . $id);
		}

		return redirect(route('raw_materials.index'))
			->with('success', _i('Raw material \'%s\' (№ %d) was deleted!', [$rawMaterialName, $id]));
	}
}
