<?php

namespace EvolveAdmin\Http\Controllers\Auth;

use EvolveAdmin\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
	function AuthRouteAPI(Request $request)
	{
		return $request->user();
	}
}
