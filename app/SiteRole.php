<?php

namespace EvolveAdmin;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteRole
 *
 * @package app
 */
class SiteRole extends Model
{
	const ADMIN_ROLE_NAME = 'administrator';

	protected $fillable = ['name'];

	public $timestamps = false;

	public function users()
	{
		return $this->belongsToMany(SiteUser::class);
	}
}