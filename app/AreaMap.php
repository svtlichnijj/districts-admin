<?php

namespace EvolveAdmin;

use Illuminate\Database\Eloquent\Model;

class AreaMap extends Model
{
	protected $fillable = [
		'scale',
		'size_x',
		'size_y',
		'map_data',
		'x',
		'y',
	];
}
