<?php

namespace EvolveAdmin;

use Illuminate\Database\Eloquent\Model;

class RawMaterial extends Model
{
    protected $fillable = [
    	'name',
    	'prevalence',
    	'skin_file_name',
    ];
}
