<?php

namespace EvolveAdmin\Tool;

abstract class ProtoTool
{
	public $name;
	public $ingredients;
	public $usableCount;
	public $count;
	public $level;
	public $placement;

	/**
	 * @param string $newName
	 *
	 * @throws \Exception
	 */
	function rename($newName = ''): void
	{
		if (empty($newName) || !is_string($newName)) {
			throw new \Exception(_i('Incorrect new name!'));
		}

		$this->name = $newName;
	}

	function levelUp()
	{
		$this->level += 1;
	}

	/**
	 * @param int $needCount
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	function use($needCount = 1)
	{
		if ($this->count < ($this->usableCount * $needCount)) {
			throw new \Exception(_i('Not enough %s of %s !', [($this->usableCount * $needCount) - $this->count, $this->name]));
		}

		$this->count -= $this->usableCount;

		return $this->count;
	}
}