<?php

namespace EvolveAdmin\Coordinates;

use Exception;

class Coordinates
{
	public $x, $y;

	/**
	 * Coordinates constructor.
	 *
	 * @param int $x
	 * @param int $y
	 *
	 * @throws Exception
	 */
	public function __construct($x, $y)
	{
		if ((empty($x) && $x != 0) || (empty($y) && $y != 0)) {
			throw new Exception(_i('Incorrect coordinate!'));
		}

		$this->x = intval($x);
		$this->y = intval($y);
	}

	/**
	 * @return int
	 */
	public function getX()
	{
		return $this->x;
	}

	/**
	 * @return int
	 */
	public function getY()
	{
		return $this->y;
	}

}