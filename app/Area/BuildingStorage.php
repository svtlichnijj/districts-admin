<?php

namespace EvolveAdmin\Area;

class BuildingStorage extends Building
{
	public $totalCount;

	public function __construct($x, $y)
	{
		parent::__construct($x, $y);
		$this->type = self::BUILDING_STORAGE;
	}
}