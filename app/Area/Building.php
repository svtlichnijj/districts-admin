<?php

namespace EvolveAdmin\Area;

class Building extends ProtoArea
{
	const BUILDING_CREATIVE = 'Creative';
	const BUILDING_INDUSTRIAL = 'Industrial';
	const BUILDING_STORAGE = 'Storage';

	public $level, $type;

	function levelUp()
	{
		$this->level += 1;
	}

	function getType()
	{
		return $this->type;
	}
}