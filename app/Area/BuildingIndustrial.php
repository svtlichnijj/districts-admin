<?php

namespace EvolveAdmin\Area;

class BuildingIndustrial extends Building
{
	public $rawMaterial;

	public function __construct($x, $y)
	{
		parent::__construct($x, $y);
		$this->type = self::BUILDING_INDUSTRIAL;
	}

}