<?php

namespace EvolveAdmin\Area;

use EvolveAdmin\Coordinates\Coordinates;
use Exception;

class AreaMap
{
	public $scale, $sizeX, $sizeY, $map, $x, $y;

	/**
	 * AreaMap constructor.
	 *
	 * @param Coordinates $coordinates
	 * @param int $sizeX
	 * @param int $sizeY
	 * @param int $scale
	 *
	 * @throws Exception
	 */
	public function __construct(Coordinates $coordinates, $sizeX, $sizeY = 0, $scale = 1)
	{
		if (get_class($coordinates) != Coordinates::class) {
			throw new Exception(_i('Incorrect coordinate for area!'));
		}

		$this->x = $coordinates->getX();
		$this->y = $coordinates->getY();

		if ((empty($sizeX) && $sizeX != 0) || $sizeX < 0) {
			throw new Exception(_i('Wrong map size!'));
		}

		$this->sizeX = $sizeX;

		if ($sizeY == 0) {
			$this->sizeY = $this->sizeX;
		} elseif (!empty($sizeY) && $sizeY > 0) {
			$this->sizeY = $sizeY;
		} else {
			throw new Exception(_i('Wrong second map size!'));
		}

		if (empty($scale)) {
			throw new Exception(_i('Wrong map scale!'));
		}

		$this->scale = $scale;

		for ($i = 0; $i < $sizeX; $i++) {
			for ($j = 0; $j < $sizeY; $j++) {
				$coordinates = new Coordinates($i, $j);
				$this->map[$coordinates->getX()][$coordinates->getY()] =
					new AreaEmpty($coordinates, $i . 'x' . $j);
			}
		}
	}
}