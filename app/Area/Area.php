<?php

namespace EvolveAdmin\Area;

use Exception;

class Area extends ProtoArea
{
	/**
	 * @param Building $building
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function canBuildNew(Building $building)
	{
		if (!empty($this->building)) {
			throw new Exception(_i('Other building exist here!'));
		}

		if (!($building->type == $building::BUILDING_INDUSTRIAL && !empty($this->raw))) {
			throw new Exception(_i('Wrong building type!'));
		}

		return true;
	}
}