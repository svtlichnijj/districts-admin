<?php

namespace EvolveAdmin\Area;

class BuildingCreative extends Building
{
	public $rawIncome, $materialOut;

	public function __construct($x, $y)
	{
		parent::__construct($x, $y);
		$this->type = self::BUILDING_CREATIVE;
	}

}