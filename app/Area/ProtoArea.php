<?php

namespace EvolveAdmin\Area;

use EvolveAdmin\Coordinates\Coordinates;
use Exception;

abstract class ProtoArea
{
	public $x, $y;
	public $raw, $building;
	public $name, $skin;

	/**
	 * ProtoArea constructor.
	 *
	 * @param Coordinates $coordinates
	 * @param string $name
	 *
	 * @throws Exception
	 */
	public function __construct(Coordinates $coordinates, $name = '')
	{
		if (get_class($coordinates) != Coordinates::class) {
			throw new Exception(_i('Incorrect coordinate for area!'));
		}

		if (empty($name)) {
			throw new Exception(_i('Wrong name!'));
		}

		$this->x = $coordinates->getX();
		$this->y = $coordinates->getY();
		$this->name = $name;
	}

	/**
	 * @param string $newName
	 *
	 * @throws Exception
	 */
	function rename($newName = '')
	{
		if (empty($newName) || !is_string($newName)) {
			throw new Exception('Wrong new name!');
		}

		$this->name = $newName;
	}

	function getCoordinate()
	{
		return [$this->x, $this->y];
	}

	function getInfo()
	{
		return $this->building ?: $this->raw;
	}

}