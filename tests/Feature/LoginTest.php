<?php

namespace Tests\Feature;

use EvolveAdmin\SiteUser;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{

	public function testUserCanViewLoginForm()
	{
		$response = $this->get('/admin/login');
		$response->assertStatus(200)
		         ->assertSuccessful()
		         ->assertViewIs('admin.login');
	}

	public function testUserCannotViewLoginFormWhenAuthenticated()
	{
		$user = factory(SiteUser::class)->make();
		$response = $this->actingAs($user)->get('/admin/login');
		$response->assertRedirect('/admin');
	}

    public function testRequiresEmailAndLogin()
    {
	    Session::start();

	    $response = $this->post('/admin/login')
	                     ->assertStatus(302)
	                     ->assertRedirect('/');

		$response->assertSessionHasErrors([
			'login'    => 'The login field is required.',
			'password' => 'The password field is required.'
		]);
    }

	public function testUserLoginUnsuccessfully()
	{
		Session::start();

		$userLoginParams = ['login' => 'test_login', 'password' => bcrypt('test_password')];

		$response = $this->post('/admin/login', $userLoginParams)
		                 ->assertStatus(302)
		                 ->assertRedirect('/');

		$response->assertSessionHasErrors(['login' => 'These credentials do not match our records.']);

		// Find out why it failed
//		dd(Session::all());
	}

	public function testUserLoginSuccessfully()
	{
		Session::start();

		$userLoginParam = ['login' => 'test_login'];
		$userLoginParams = $userLoginParam;
		$userLoginParams['remember_token'] = csrf_token();
		$userPassword = 'test_password';
		$userParams = array_merge($userLoginParams, ['password' => bcrypt($userPassword)]);

		SiteUser::where($userLoginParam)->delete();
		$user = factory(SiteUser::class)->create($userParams);

		$userParams['password'] = $userPassword;
		$response = $this->post('/admin/login', $userParams)
		                 ->assertRedirect('/admin');

		$user->delete();
		$response->assertSessionMissing('errors');

		// Find out why it failed
//		dd(Session::get('errors'));
	}
}
