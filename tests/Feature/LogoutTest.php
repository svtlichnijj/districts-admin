<?php

namespace Tests\Feature;

use EvolveAdmin\SiteUser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogoutTest extends TestCase
{
	public function testUserIsLoggedOutProperly()
	{
		$userParams = ['login' => 'user_test', 'password' => bcrypt('password_test')];
		factory(SiteUser::class)->make($userParams);

		$this->post('/admin/login', $userParams)->assertStatus(302);
		$this->post('/admin/logout')->assertStatus(302);
		$this->get('/admin')->assertRedirect('/admin/login');
	}
/*
	public function testUserWithNullToken()
	{
		// Simulating login
		$user = factory(User::class)->create(['email' => 'user@test.com']);
		$token = $user->generateToken();
		$headers = ['Authorization' => "Bearer $token"];

		// Simulating logout
		$user->api_token = null;
		$user->save();

		$this->get('/admin', $headers)->assertStatus(401);
	}
*/}
