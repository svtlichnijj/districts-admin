@extends('admin.layouts._layout')

@push('custom_styles')
@endpush

@push('custom_scripts')
@endpush

@section('breadcrumbs')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">{{ _i('Article Creation') }}</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">{{ _i('Admin panel') }}</a></li>
                <li class="breadcrumb-item active">{{ _i('Article Creation') }}</li>
            </ol>
        </div>
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="basic-form">
                    {{ _i('Dashboard') }}
                </div>
            </div>
        </div>
    </div>
@endsection