<div class="scroll-sidebar">
    <a class="nav-link pl-1" href="{{ route('area_maps.index') }}">
        <i class="far fa-map"></i> {{ _i('Area maps') }}
    </a>
    <a class="nav-link pl-1" href="#" data-toggle="collapse" data-target="#submenu_areas" role="button" aria-expanded="true">
        <i class="fab fa-wpforms"></i> {{ _i('Area') }}
    </a>
    <div class="collapse show" id="submenu_areas">
        <ul class="flex-column pl-2 nav">
            <li class="nav-item py-1">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#raw-materials" role="button" aria-expanded="false">{{ _i('Raw materials') }}</a>
                <div class="collapse" id="raw-materials">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item">
                            <a class="nav-link py-0" href="{{ route('raw_materials.index') }}">{{ _i('Raw materials') }}</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 collapsed" href="#" data-toggle="collapse" data-target="#buildings" role="button" aria-expanded="false">{{ _i('Buildings') }}</a>
                <div class="collapse" id="buildings">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item">
                            <a class="nav-link py-0" href="{{ url('storage-buildings.create') }}">{{ _i('Storage buildings') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-0" href="{{ url('industrial-buildings.create') }}">{{ _i('Industrial buildings') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-0" href="{{ url('creative-buildings.create') }}">{{ _i('Creative buildings') }}</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
            <div class="dropdown-divider"></div>
    <div class="collapse show" id="submenu2">
        {{ trans_choice('Example', 2) }}
        <ul class="flex-column pl-2 nav">
            <li class="nav-item">
                <a class="nav-link collapsed py-0" href="#submenu1sub1" data-toggle="collapse" data-target="#submenu1sub1">
                    Customers
                </a>
                <div class="collapse small" id="submenu1sub1" aria-expanded="false">
                    <ul class="flex-column nav pl-4">
                        <li class="nav-item">
                            <a class="nav-link p-0" href="">
                                <i class="fa fa-fw fa-clock"></i> Daily
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link p-0" href="">
                                <i class="fa fa-fw fa-tools"></i> Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link p-0" href="">
                                <i class="fa fa-fw fa-chart-bar"></i> Charts
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link p-0" href="">
                                <i class="fa fa-fw fa-compass"></i> Areas
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>