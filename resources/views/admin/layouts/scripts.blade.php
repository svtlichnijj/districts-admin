<script src="{{ asset('/backend/jquery/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('/backend/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/admin/js/scripts.js') }}"></script>

@stack('custom_scripts')