<div class="row">
    <div class="col-12 text-center">
        <a href="https://github.com/puikinsh/ElaAdmin">Ela Admin</a> © {{ date('Y') }} {{ _i('All rights reserved') }}.
    </div>
</div>