<div class="header">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('admin.dashboard')  }}">
                <b>
                    <img src="{{ asset('/admin/images/logo.png') }}" alt="homepage" class="dark-logo"/>
                </b>
                <span>
                    <img src="{{ asset('/admin/images/logo-text.png') }}" alt="homepage" class="dark-logo"/>
                </span>
            </a>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto mt-md-0">
                <li class="nav-item">
                    <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)">
                        <i class="mdi mdi-menu">1717</i>
                    </a>
                </li>
                <li class="nav-item m-l-10">
                    <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)">
                        <i class="ti-menu">2222</i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-muted" href="{{ url('/') }}"><i class="ti-home"></i>
                        <span class="hidden-sm-down">{{ _i('Go to main page') }}</span>
                    </a>
                </li>
            </ul>
            <div class="navbar-nav my-lg-0">
                <div class="btn-group">
                    <a class="btn btn-outline-warning btn-sm" href="#">
                        <i class="fa fa-user"></i>
                        <img src="{{ asset('/admin/images/user.jpg') }}" alt="user" class="profile-pic"/>
                    </a>
                    <button type="button" class="btn btn-outline-info btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">{{ _i('User menu') }}</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-divider"></div>
                        <a class="fa fas fa-sign-out-alt dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Выход
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
