<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.meta')

    @include('admin.layouts.styles')
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/admin/images/favicon.png') }}">
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/fontawesome.min.css">--}}
    <title>Laravel Ela Admin</title>
</head>
<body class="fix-header fix-sidebar">
<!--<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>-->

<div id="main-wrapper" class="container">
    @include('admin.layouts.header')

    <div class="page-wrapper container-fluid">
        <div class="row">
            <div class="left-sidebar col-3 bg-light">
                @include('admin.layouts.sidebar')
            </div>
            <div class="col-9">
                @yield('breadcrumbs')
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        @include('admin.layouts.footer')
    </footer>
</div>

@include('admin.layouts.scripts')

</body>
</html>