<link href="{{ asset('/backend/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/fontawesome/css/all.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style-admin.css') }}" rel="stylesheet">

@stack('custom_styles')