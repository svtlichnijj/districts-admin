@extends('admin.layouts._layout')

@section('breadcrumbs')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">{{ _i('All raw materials')  }}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')  }}">{{ _i('Admin start page') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('raw_materials.index')  }}">{{ _i('Raw materials') }}</a></li>
            </ol>
        </div>

        <div class="col-sm-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <div>
        <a href="{{ route('raw_materials.create')}}" class="btn btn-primary m-3">{{ _i('New raw material') }}</a>
    </div>
    <table class="table table-striped">
    <thead>
    <tr>
        <td>{{ _i('ID') }}</td>
        <td>{{ _i('Name') }}</td>
        <td>{{ _i('Prevalence') }}</td>
        <td>{{ _i('Skin') }}</td>
        <td colspan = 2>{{ _i('Actions') }}</td>
    </tr>
    </thead>
    <tbody>
    @foreach($rawMaterials as $rawMaterial)
        <tr>
            <td>{{ $rawMaterial->id }}</td>
            <td>{{ $rawMaterial->name }}</td>
            <td>{{ $rawMaterial->prevalence }} %</td>
            <td>
                <img src="{{ asset('/public/raw-material/' . $rawMaterial->id . '/' . $rawMaterial->skin_file_name) }}"
                     alt="{{ $rawMaterial->skin_file_name }}" class="image-preview-list"/>
            </td>
            <td>
                <a href="{{ route('raw_materials.edit', $rawMaterial->id) }}" class="btn btn-primary">{{ _i('Edit') }}</a>
            </td>
            <td>
                <form action="{{ route('raw_materials.destroy',
                [$rawMaterial->id, 'name' => $rawMaterial->name, 'skin_file_name' => $rawMaterial->skin_file_name]) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">{{ _i('Delete') }}</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection