@extends('admin.layouts._layout')

@section('breadcrumbs')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">{{ _i('Edit raw material') }}</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">{{ _i('Admin start page') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('raw_materials.index') }}">{{ _i('Area maps') }}</a></li>
                <li class="breadcrumb-item active">{{ _i('Edit') }}</li>
            </ol>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
    </div>
@stop

@section('content')
<div class="basic-form">
    <form method="POST" action="{{ route('raw_materials.update', $rawMaterial->id) }}" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-group form-row">
            <div class="form-inline col-lg-6">
                <label for="name" class="col-lg-4 col-form-label control-label justify-content-start">{{ _i('Name') }}</label>
                <input type="text" name="name" id="name" class="col-lg-8 form-control"
                       value="{{ $rawMaterial->name }}"
                       placeholder="{{ _i('Name of raw material') }}" required>
            </div>
        </div>

        <div class="form-group form-row">
            <div class="form-inline col-lg-6">
                <label for="prevalence" class="col-lg-4 col-form-label control-label justify-content-start">{{ _i('Prevalence') }}</label>
                <input type="range" min="1" name="prevalence-r" id="prevalence-r" class="col-lg-5 form-control p-0"
                       value="{{ $rawMaterial->prevalence }}" onchange="valueFromTo(this, 'prevalence')"
                       aria-describedby="prevalenceHelp">
                <input type="number" name="prevalence" id="prevalence" min="1" max="100" class="col-lg-2 p-0"
                       value="{{ $rawMaterial->prevalence }}" onchange="valueFromTo(this, 'prevalence-r')" required>
                <div class="col-lg-1">%</div>
                <small id="prevalenceHelp" class="form-text text-muted">{{ _i('Percent prevalence, uniqueness') }}</small>
            </div>
        </div>

        <div class="form-group form-row">
            <div class="col-lg-12">
                <h4>{{ _i('View') }}</h4>
            </div>
            <div class="form-group col-lg-6">
                <div class="form-inline">
                    <label for="skin_file" class="col-lg-12 control-label">{{ _i('Skin') }}</label>
                    <input type="file" name="skin_file" id="skin_file" class="col-lg-12 form-control p-0 border-0"
                           placeholder="{{ _i('Skin') }}" onchange="readURL(this)" aria-describedby="skinFileHelp">
                    <small id="skinFileHelp" class="form-text text-muted">{{ _i('How raw materials should look') }}</small>
                </div>
                <div class="form-group">
                    <label for="skin_file_name" class="col-lg-4 col-form-label control-label justify-content-start">{{ _i('Skin name') }}</label>
                    <input type="text" name="skin_file_name" id="skin_file_name" class="col-lg-8 form-control"
                           value="{{ $rawMaterial->skin_file_name }}" aria-describedby="skinFileNameHelp">
                    <small id="skinFileNameHelp" class="form-text text-muted">{{ _i('Can be blank and can be retrieved from a file name') }}</small>
                </div>
            </div>
            <div class="form-group col-lg-6">
                <div class="form-inline col-lg-12">
                    <img alt="{{ $rawMaterial->skin_file_name }}" src="{{ asset('/public/raw-material/' . $rawMaterial->id . '/' . $rawMaterial->skin_file_name) }}"
                         id="raw-material-img-tag" class="mw-100" />
                </div>
            </div>
        </div>

        <input type="hidden" name="skin_file_name_old" value="{{ $rawMaterial->skin_file_name }}">
        <input type="submit" class="btn btn-info float-lg-right" value="{{ _i('Edit area map') }}">
    </form>
</div>
@endsection

@push('custom_scripts')
    <script>
        function valueFromTo(from, idTo) {
            $($("#" + idTo)).val($(from).val());
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                const reader = new FileReader();

                reader.onload = function (e) {
                    $('#raw-material-img-tag').attr('src', e.target.result);
                    $('#skin_file_name').val('');
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            if ('{{ $rawMaterial->skin_file_name }}' !== '') {
                $('#skin_file_name').val('{{ $rawMaterial->skin_file_name }}');
            }
        });
    </script>
@endpush