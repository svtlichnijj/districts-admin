@extends('admin.layouts._layout')

@section('breadcrumbs')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">{{ _i('Edit area map') }}</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">{{ _i('Admin start page') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('area_maps.index') }}">{{ _i('Area maps') }}</a></li>
                <li class="breadcrumb-item active">{{ _i('Edit') }}</li>
            </ol>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
    </div>
@stop

@section('content')
<div class="basic-form">
    <form method="POST" action="{{ route('area_maps.update', $areaMap->id) }}">
        @method('PUT')
        @csrf
        <div class="form-group form-row">
            <div class="col-lg-12">
                <h4>{{ _i('Scale of the game level') }}</h4>
            </div>
            <div class="form-inline col-lg-6 text-center">
                <label for="scale" class="col-lg-3 control-label">{{ _i('Scale') }}</label>
                <input type="number" min="1" value="{{ $areaMap->scale }}" class="form-control" id="scale" name="scale"
                       placeholder="{{ _i('Scale number') }}" aria-describedby="scaleHelp">
                <small id="scaleHelp" class="form-text text-muted">{{ _i('Scale equal of game level') }}</small>
            </div>
        </div>

        <div class="form-group form-row">
            <div class="col-lg-12">
                <h4>{{ _i('Sizes') }}</h4>
            </div>
            <div class="form-inline col-lg-6 text-center">
                <label for="size_x" class="col-lg-3 col-form-label control-label">{{ _i('Width') }}</label>
                <input type="number" min="1" value="{{ $areaMap->size_x }}" class="form-control" id="size_x" name="size_x"
                       placeholder="{{ _i('Width of area map') }}">
            </div>
            <div class="form-inline col-lg-6 text-center">
                <label for="size_y" class="col-lg-3 col-form-label control-label">{{ _i('Height') }}</label>
                <input type="number" min="1" value="{{ $areaMap->size_y }}" class="form-control" id="size_y" name="size_y"
                       placeholder="{{ _i('Height of area map') }}">
            </div>
        </div>

        <div class="form-group form-row">
            <div class="col-lg-12">
                <h4>{{ _i('Coordinates') }}</h4>
            </div>
            <div class="form-inline col-lg-6 text-center">
                <label for="coordinate_x" class="col-lg-3 control-label">{{ _i('Start x') }}</label>
                <input type="number" min="0" value="{{ $areaMap->x }}" class="form-control" id="coordinate_x" name="x"
                       placeholder="{{ _i('Start x coordinate') }}">
            </div>
            <div class="form-inline col-lg-6 text-center">
                <label for="coordinate_y" class="col-lg-3 control-label">{{ _i('Start y') }}</label>
                <input type="number" min="0" value="{{ $areaMap->y }}" class="form-control" id="coordinate_y" name="y"
                       placeholder="{{ _i('Start y coordinate') }}">
            </div>
        </div>

        <input type="submit" class="btn btn-info float-lg-right" value="{{ _i('Edit area map') }}">
    </form>
</div>
@endsection