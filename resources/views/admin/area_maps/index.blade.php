@extends('admin.layouts._layout')

@section('breadcrumbs')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">{{ _i('Create area map')  }}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')  }}">{{ _i('Admin start page') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('area_maps.index')  }}">{{ _i('Area maps')  }}</a></li>
            </ol>
        </div>

        <div class="col-sm-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <div>
        <a style="margin: 19px;" href="{{ route('area_maps.create')}}" class="btn btn-primary">{{ _i('New area map') }}</a>
    </div>
    <table class="table table-striped">
    <thead>
    <tr>
        <td>{{ _i('ID') }}</td>
        <td>{{ _i('Scale') }}</td>
        <td>{{ _i('Sizes') }}</td>
        <td>{{ _i('Coordinates') }}</td>
        <td colspan = 2>{{ _i('Actions') }}</td>
    </tr>
    </thead>
    <tbody>
    @foreach($areaMaps as $areaMap)
        <tr>
            <td>{{ $areaMap->id }}</td>
            <td>{{ $areaMap->scale }}</td>
            <td>{{ $areaMap->width }} x {{ $areaMap->height }}</td>
            <td>{{ $areaMap->x }} : {{ $areaMap->y }}</td>
            <td>
                <a href="{{ route('area_maps.edit',$areaMap->id) }}" class="btn btn-primary">{{ _i('Edit') }}</a>
            </td>
            <td>
                <form action="{{ route('area_maps.destroy', $areaMap->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">{{ _i('Delete') }}</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection