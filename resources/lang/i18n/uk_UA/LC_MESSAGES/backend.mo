��    C      4  Y   L      �     �     �     �     �     �     �            	   3     =  	   N  2   X     �     �     �     �     �  	   �     �     �     �     �               2     B     I     g     j     �     �     �     �     �     �     �     �     �            
   ;     F  )   a  #   �  '   �     �     �     �     �      	     	     2	     8	  	   =	     G	     O	     W	  	   i	     s	     x	     ~	     �	     �	     �	     �	  
   �	  �  �	     �  )   �  <   �  ,   *  "   W     z  %   �  .   �     �     �       c        ~     �  *   �  6   �  (        /     <     M  .   b  :   �  2   �  #   �  3   #     W  /   d     �  3   �  (   �  (   �       
   &  
   1  *   <  6   g      �  6   �     �  A        E  >   \  L   �  G   �  N   0  %        �      �  
   �  ;   �  ;   "     ^     m     |     �     �  *   �     �            (   )  (   R  *   {      �  7   �  
   �           .      "   @                      C      
      -          '       ?   3         A   #                   	            B   0   ;      <   &   8   =            9   4           /           $      !                         2   *       7      +   %       >          :      )   6             1           ,   5           (    Actions Admin panel Admin start page All raw materials All rights reserved Area Area map '%d' updated! Area map '%d' was deleted! Area maps Article Creation Buildings Can be blank and can be retrieved from a file name Coordinates Create Create area map Create raw material Creative buildings Dashboard Delete Edit Edit area map Edit raw material Example Examples Forgot Your Password? Go to main page Height How raw materials should look ID Incorrect coordinate for area! Incorrect new name! Industrial buildings Login Logout Name New area map New raw material Not enough %s of %s ! Other building exist here! Password Percent prevalence, uniqueness Prevalence Raw material '%d' updated! Raw material '%s' (№ %d) can not found! Raw material '%s' (№ %d) created! Raw material '%s' (№ %d) was deleted! Raw materials Register Remember Me Scale Scale equal of game level Scale of the game level Sizes Skin Skin name Start x Start y Storage buildings User menu View Width Wrong building type! Wrong map scale! Wrong map size! Wrong name! Wrong second map size! Your login Project-Id-Version: Evolve admin
PO-Revision-Date: 2021-05-10 20:12+0300
Last-Translator: Taras Svitlychnyi <svtlichnijj@gmail.com>
Language-Team: Taras Svitlychnyi <svtlichnijj@gmail.com>
Language: uk_UA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-KeywordsList: _;__;_i;_s;gettext;_n:1,2;ngettext:1,2;dgettext:2
X-Poedit-Basepath: ../../../../../app
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: Http/Controllers/Admin
X-Poedit-SearchPath-1: Area
X-Poedit-SearchPath-2: Tool
X-Poedit-SearchPath-3: ../resources/views/admin
 Дії Панель адміністратора Початкова панель адміністратора Всі початкові матеріали Всі права захищені Ділянка Ділянка '%d' оновлена! Ділянка '%d' була видалена! Ділянка Створення статті Будови Може бути порожнім і його можна отримати з імені файлу Координати Створити Створити карту ділянки Створити початковий матеріал Будівлі для створення Панель Видалити Редагувати Редагувати карту ділянки Редагувати початкові матеріали Приклад Приклади Прикладів Забули свій пароль? Перейти на головну сторінку Висота Як має виглядати сировина ID Невірні координати ділянки! Не коректне нове імʼя! Будівлі з виробництва Вхід Вийти Назва Створити карту ділянки Створити початковий матеріал Не вистачає %s з %s ! Інша будівля знаходиться тут! Пароль Відсоток поширеності, унікальність Поширеність Початковий матеріал '%d' оновлений! Початковий матеріал '%s' (№ %d) не знайдений! Початковий матеріал '%s' (№ %d) створений! Початковий матеріал '%s' (№ %d) був видалений! Початкові матеріали Реєстрація Запам’ятати мене Шкала Шкала відповідає ігровому рівню Шкала відповідає ігровому рівню Розміри Малюнок Назва малюнка Початковий X Початковий Y Будівлі для зберігання Меню користувача Вигляд Ширина Не вірний тип будівлі! Не вірний шкала карти! Не вірний розмір карти! Неправильне імʼя! Не вірний другий розмір карти! Логін 