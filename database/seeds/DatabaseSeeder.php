<?php

use EvolveAdmin\SiteUser;
use EvolveAdmin\SiteRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{

	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		// $this->call(UsersTableSeeder::class);
		$this->call('SiteRoleTableSeeder');
		$this->command->info('Таблица ролей заповнена даними!');
		$this->call('UserTableSeeder');
		$this->command->info('Таблица користувачів заповнена даними!');
		$this->call('SiteRoleUserTableSeeder');
		$this->command->info('Таблица-звʼязка ролей-користувачів заповнена даними!');
	}
}

class SiteRoleTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('site_roles')->delete();

		SiteRole::create(['name' => SiteRole::ADMIN_ROLE_NAME]);
	}
}

class UserTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('site_users')->delete();

		SiteUser::create([
		    'login' => SiteUser::ADMIN_USER_LOGIN,
		    'email' => 'svtlichnijj@gmail.com',
		    'password' => bcrypt('password'),
		    'created_at' => date('Y-m-d H:i:s'),
		    'updated_at' => date('Y-m-d H:i:s'),
	    ]);
    }
}

class SiteRoleUserTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('site_role_site_user')->delete();

		SiteUser::where('login', '=', SiteUser::ADMIN_USER_LOGIN)->firstOrFail()->siteRoles()
			->attach(SiteRole::where('name', '=', SiteRole::ADMIN_ROLE_NAME)->first()->getKey());
	}
}
