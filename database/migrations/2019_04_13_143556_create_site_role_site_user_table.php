<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteRoleSiteUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_role_site_user', function (Blueprint $table) {
	        $table->tinyInteger('site_role_id')->unsigned();
	        $table->bigInteger('site_user_id')->unsigned();

	        $table->foreign('site_role_id')->references('id')->on('site_roles')->onDelete('cascade');
	        $table->foreign('site_user_id')->references('id')->on('site_users')->onDelete('cascade');

	        $table->primary(['site_user_id', 'site_role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_role_site_user');
    }
}
