<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_maps', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('size_x');
            $table->integer('size_y');
            $table->tinyInteger('scale');
            $table->text('map_data');
	        $table->integer('x');
	        $table->integer('y');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_maps');
    }
}
