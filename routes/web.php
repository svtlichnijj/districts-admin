<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// Admin routes
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
	Route::get('login', 'LoginController@index');
	Route::post('login', 'LoginController@login')->name('admin.login');
	Route::post('logout', 'LoginController@logout')->name('logout');

	Route::group(['middleware' => 'admin'], function() {
		Route::get('/', 'DashboardController@index')->name('admin.dashboard');
		Route::resources([
			'area_maps' => 'AreaMapController',
			'raw_materials' => 'RawMaterialController'
		]);
	});
});

// Site routes
Route::group(['prefix' => '', 'namespace' => 'Site'], function() {
	Route::get('/', 'HomeController@index')->name('homepage');
});